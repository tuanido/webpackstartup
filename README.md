# Webpack startup #

Angular + bootstrap + typescript + webpack

# Global dependencies:
* $ npm install -g typings

# To Install:
* $ npm install

# To pack:
* $ webpack

# To start server:
* $ npm start

# To see the app:
*Enter to http://localhost:8080