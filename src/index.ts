import * as angular from "angular";
import {MainController} from "./maincontroller";

angular.module("myApp", []).controller('MainController', MainController);
