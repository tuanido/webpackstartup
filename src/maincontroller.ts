import * as angular from "angular"

export class MainController {
  static $inject: Array<string> = [];

  public state : string;

  constructor() {
    this.state = "Inicializado";
  }

  public changeStatusToHello() {
    this.state = "Estado cambiado !!!!";
  }
}
